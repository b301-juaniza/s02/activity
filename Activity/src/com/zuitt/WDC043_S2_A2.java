package com.zuitt;

import java.util.*;

public class WDC043_S2_A2 {
    public static void main(String [] args) {
        int [] prime = new int[5];
        int index = 0;
        for (int i = 1; i < 100; i++){
            if (prime[4] == 0){
                if (i % 1 == 0 && i % i == 0 && i != 1) {
                    if (i%2 != 0 && i%3 != 0 || i == 3 && i%4 != 0 && i%5 != 0 || i == 5 || i == 2){
                        prime[index] = i;
                        index += 1;
                    }
                }
            } else {
                break;
            }
        }
        System.out.println(Arrays.toString(prime));
        System.out.println("The first prime number is: " + prime[0]);

        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My Friends are: "+ friends);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put("Toothpaste", 15);
        inventory.put("Toothbrush", 20);
        inventory.put("Soap", 12);
        System.out.println("Our current inventory consist of: "+ inventory);
    }
}
