package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {
    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);

        for (int i = 0; i < 2; i++){
            System.out.print("Enter Year: ");
            int year = scan.nextInt();

            if (year % 4 == 0) {
                System.out.println(year + " is a leap year");
            } else {
                System.out.println(year + " is not a leap year");
            }
        }
    }
}
