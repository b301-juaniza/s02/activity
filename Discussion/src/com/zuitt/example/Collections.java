package com.zuitt.example;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Collections {
    public static void main (String[] args){
//        Arrays are fixed-data structures that store a collection of elements of the same type
        int [] intArray = new int[5];
        System.out.println("Initial state of intArray: ");
        System.out.println(Arrays.toString(intArray));
        intArray[0] = 1;
        intArray[intArray.length-1] = 6;
        System.out.println("Updated state of intArray: ");
        System.out.println(Arrays.toString(intArray));

//        Multidimensional Arrays
//        3 Rows , 3 Column
        String [][] classroom = new String[3][3];

//        First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

//        Second Row
        classroom[1][0] = "Brandom";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";

//        Second Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom)+ "\n");
        for (String [] x: classroom) {
            System.out.println(Arrays.toString(x));
        }
        System.out.println("-----------------");
        ArrayList<String> students = new ArrayList<String>();
        System.out.println(students);

        students.add("John");
        students.add("Paul");
        System.out.println(students.size());
        System.out.println(students.get(0) + " " + students.get(1));
        System.out.println(students);
        students.set(1, "George");
        System.out.println(students);
        students.remove(1);
        System.out.println(students);
        students.clear();
        System.out.println(students);

//        HashMaps
        System.out.println("-----------------");
        HashMap<String, String> job_position = new HashMap<String, String>();
//                          Key       Value
        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");
        System.out.println(job_position);
        System.out.println(job_position.get("Alice"));
//        job_position.remove("Brandon");
//        System.out.println(job_position);
        System.out.println(job_position.keySet());
        job_position.replace("Alice", "Secretary");
        System.out.println(job_position);
    }
}
