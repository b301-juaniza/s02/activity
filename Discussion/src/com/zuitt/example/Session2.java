package com.zuitt.example;
import java.util.Scanner;

public class Session2 {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
//        If Else else-if Statement
        int num = 10;
        if (num > 0) {
            System.out.println("The number is Positive");
        } else if (num < 0) {
            System.out.println("The number is Negative");
        } else {
            System.out.println("The number is 0");
        }
//        Switch Case Statement
        System.out.print("Enter a number: ");
        int x = scan.nextInt();
        switch (x) {
            case 1: {
                System.out.println("This is Number One");
                break;
            }
            case 2: {
                System.out.println("This is Number Two");
                break;
            }
            case 3: {
                System.out.println("This is Number Three");
                break;
            }
            case 4: {
                System.out.println("This is Number Four");
                break;
            }
            case 5: {
                System.out.println("This is Number Five");
                break;
            }
            default: {
                System.out.println("The Number is Greater than Five");
                break;
            }
        }

    }
}
